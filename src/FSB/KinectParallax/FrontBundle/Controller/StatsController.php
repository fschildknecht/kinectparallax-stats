<?php

namespace FSB\KinectParallax\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use FSB\KinectParallax\CoreBundle\Entity\Stat;
use FSB\KinectParallax\CoreBundle\Entity\Swipe;

class StatsController extends Controller
{
    public function indexAction()
    {
        $request = $this->getRequest();
        $delimiter = ';';
        $content = implode(";", array('Start Time', 'Usage Time (sec)', 'Usage Time (min and sec)', 'Number of swipes', 'Type')) . "\r\n";

        $em = $this->getDoctrine()->getEntityManager();
        $stats = $em->getRepository('FSBKinectParallaxCoreBundle:Stat')->findAll();

        $startDate = new \DateTime('2013-05-14');
        $endDate = new \DateTime('2013-05-18');

        foreach ($stats as $stat) {
            $stat->getStarttime()->setTimestamp($stat->getStarttime()->getTimestamp() + 2 * 3600);
            $stat->getEndtime()->setTimestamp($stat->getEndtime()->getTimestamp() + 2 * 3600);
            if (($stat->getStarttime()->getTimestamp() < $endDate->getTimestamp()) && ($stat->getStarttime()->getTimestamp() > $startDate->getTimestamp())) {
                $starttime = $stat->getStarttime();
                $endtime = $stat->getEndtime();
                $seconds = $endtime->getTimestamp() - $starttime->getTimestamp();
                $timing = floor($seconds / 60) . 'min and ' . $seconds % 60 . 'sec';
                $swipes = count($stat->getSwipes());
                $type = 'static';
                if ($stat->getParallax() === true) {
                    if ($stat->getHand() === true) {
                        $type = 'parallax / hand';
                    } else {
                        $type = 'parallax';
                    }
                } else {
                    if ($stat->getHand() === true) {
                        $type = 'hand';
                    } else {
                        $type = 'static';
                    }
                }

                $lineData = array(
                    $starttime->format('Y-m-d H:i:s'),
                    $seconds,
                    $timing,
                    $swipes,
                    $type
                );

                $content .= implode(";", array_values($lineData)) . "\r\n";
            }
        }

        $response = new Response($content);

        $response->headers->set('Content-Type', 'text/plain; charset=iso-8859-1');
        $response->headers->set('Content-Disposition', 'attachment; filename="export-'.uniqid().'.csv"');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        return $response;
    }
}
