<?php

namespace FSB\KinectParallax\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use FSB\KinectParallax\CoreBundle\Entity\Stat;
use FSB\KinectParallax\CoreBundle\Entity\Swipe;

class RestController extends Controller
{
    public function indexAction()
    {
    	$request = $this->getRequest();
    	$response = array();
    	$status = 200;

    	$em = $this->getDoctrine()->getEntityManager();
    	$stats = $em->getRepository('FSBKinectParallaxCoreBundle:Stat')->findAll();

        foreach ($stats as &$stat) {
            $stat = $stat->toArray();
        }

    	$response['stats'] = $stats;

    	return $this->generateJsonResponse($response, $status);
    }

    public function createAction()
    {
    	$request = $this->getRequest();
    	$em = $this->getDoctrine()->getEntityManager();
    	$response = array();
    	$status = 200;

    	if ($request->getMethod() === 'POST') {
    		$dataStat = $request->get('stats');
    		if ($dataStat) {
    			$stat = new Stat();
    			$stat->setUser($dataStat['userId']);
                $startTime = \DateTime::createFromFormat('U', (int) $dataStat['starttime']);
    			$stat->setStarttime($startTime);
                $endTime = \DateTime::createFromFormat('U', (int) $dataStat['endtime']);
    			$stat->setEndtime($endTime);
                $stat->setParallax($dataStat['parallax'] == 'true' ? true : false);
                $stat->setHand($dataStat['hand'] == 'true' ? true : false);
    			$em->persist($stat);
                if (isset($dataStat['swipes'])) {
                    foreach ($dataStat['swipes'] as $dataSwipe) {
                        $swipe = new Swipe();
                        $swipe->setStat($stat);
                        $dateTime = \DateTime::createFromFormat('U', (int) $dataSwipe['datetime']);
                        $swipe->setDatetime($dateTime);
                        $em->persist($swipe);
                    }
                }
    			$em->flush();
    			$response['stats'] = $stat->toArray();
    		} else {
    			$status = 400;
    			$response['errors']['parameter']['stats'] = $dataStat;
    		}
    	} else {
    		$status = 400;
    		$response['errors']['query'] = $request->getMethod();
    	}

    	return $this->generateJsonResponse($response, $status);
    }

    private function generateJsonResponse($data, $statusCode = 200)
    {
    	return new JsonResponse($data, $statusCode);
    }
}
