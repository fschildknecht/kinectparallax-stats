<?php

namespace FSB\KinectParallax\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Stat
 *
 * @ORM\Table(name="stats")
 * @ORM\Entity(repositoryClass="FSB\KinectParallax\CoreBundle\Entity\StatRepository")
 */
class Stat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starttime", type="datetime")
     */
    private $starttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endtime", type="datetime")
     */
    private $endtime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="parallax", type="boolean")
     */
    private $parallax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hand", type="boolean")
     */
    private $hand;

    /**
     * @var Swipe
     *
     * @ORM\OneToMany(targetEntity="Swipe", mappedBy="stat")
     */
    private $swipes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->swipes = new ArrayCollection();
        $this->parallax = true;
        $this->hand = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @return Stat 
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get starttime
     *
     * @return \DateTime 
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Set starttime
     *
     * @return Stat 
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;

        return $this;
    }

    /**
     * Get endtime
     *
     * @return \DateTime 
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Set endtime
     *
     * @return Stat 
     */
    public function setEndtime($endtime)
    {
        $this->endtime = $endtime;

        return $this;
    }

    /**
     * Get parallax
     *
     * @return boolean
     */
    public function getParallax()
    {
        return $this->parallax;
    }

    /**
     * Set parallax
     *
     * @return Stat 
     */
    public function setParallax($parallax)
    {
        $this->parallax = $parallax;

        return $this;
    }

    /**
     * Get hand
     *
     * @return boolean
     */
    public function getHand()
    {
        return $this->hand;
    }

    /**
     * Set hand
     *
     * @return Stat 
     */
    public function setHand($hand)
    {
        $this->hand = $hand;

        return $this;
    }

    /**
     * Get swipes
     *
     * @return ArrayCollection
     */
    public function getSwipes()
    {
        return $this->swipes;
    }

    /**
     * Add a Swipe to swipes
     *
     * @return Stat
     */
    public function addSwipe($swipe)
    {
        $this->swipes->add($swipe);

        return $this;
    }

    /**
     * Remove a Swipe from swipes
     *
     * @return Stat
     */
    public function removeSwipe($swipe)
    {
        $this->swipes->remove($swipe);

        return $this;
    }

    /**
     * toString helper method
     *
     * @return Swipe
     */
    public function toString()
    {
        return $this->user().' : '.$this->getStarttime()->toString().' --- '.$this->getEndtime()->toString();
    }

    /**
     * toArray helper method
     *
     * @return array
     */
    public function toArray()
    {
        $swipes = array();
        
        foreach ($this->swipes as $swipe) {
            $swipes[] = $swipe->toArray();
        }

        return array(
            'id' => $this->getId(),
            'user' => $this->getUser(),
            'starttime' => $this->getStarttime(),
            'endtime' => $this->getEndtime(),
            'parallax' => $this->getParallax(),
            'hand' => $this->getHand(),
            'swipes' => $swipes
        );
    }
}
