<?php

namespace FSB\KinectParallax\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Swipe
 *
 * @ORM\Table(name="swipes")
 * @ORM\Entity(repositoryClass="FSB\KinectParallax\CoreBundle\Entity\SwipeRepository")
 */
class Swipe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Stat
     *
     * @ORM\ManyToOne(targetEntity="Stat", inversedBy="swipes")
     */
    private $stat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;


    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get stat
     *
     * @return Stat 
     */
    public function getStat()
    {
        return $this->stat;
    }

    /**
     * Set stat
     *
     * @return Swipe
     */
    public function setStat($stat)
    {
        $this->stat = $stat;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set datetime
     *
     * @return Swipe
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * toString helper method
     *
     * @return Swipe
     */
    public function toString()
    {
        return $this->getStat().' - '.$this->getDatetime()->toString();
    }

    /**
     * toArray helper method
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'stat' => $this->getStat()->getId(),
            'datetime' => $this->getDatetime()
        );
    }
}
