<?php

namespace FSB\KinectParallax\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('FSBKinectParallaxFrontBundle:Home:index.html.twig');
    }
}
